import json

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect, resolve_url
from django.template.response import TemplateResponse
from django.urls import reverse

from saleor.product.models import Category
from ..cart.models import Cart
from ..cart.utils import get_or_empty_db_cart
from ..product.models import Product


def index(request):
    products_list = Product.objects.all()
    page = request.GET.get('page', 1)
    paginator = Paginator(products_list, 10)

    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)

    return TemplateResponse(request, 'eddm/eddm.html', {'products': products})


@get_or_empty_db_cart(cart_queryset=Cart.objects.all())
def direction(request, cart):
    all_lines = cart.lines.all()
    if len(all_lines) == 0:
        pass
    else:
        if len(all_lines) > 1:
            # If exists more than one element, request a cart reset
            return TemplateResponse(request, 'eddm/eddm.html')
        else:
            # If exists one product, check if this is a unresolved eddm for fallback process.
            lines = [line for line in all_lines if line.data.get('is_eddm', False)
                     and not line.data.get('eddm_resolved', False)]

            if len(lines) == 0:
                # Current product is a not EDDM product or this has already resolved
                return TemplateResponse(request, 'eddm/eddm.html')

    category = Category.objects.filter(slug='full-service-eddm').first()
    path = category.get_full_path()
    category_url = reverse('product:category', kwargs={'path': path, 'category_id': category.id})
    return TemplateResponse(request, 'eddm/direction.html', {'eddm_url': category_url})


@get_or_empty_db_cart(cart_queryset=Cart.objects.all())
def reset(request, cart):
    cart.clear()
    request.session.pop('eddm_info', None)
    return redirect('eddm:direction')


@get_or_empty_db_cart(cart_queryset=Cart.objects.all())
def order(request, cart):
    """
    Apply selected routes to product. This is a AJAX request only.
    :param request:
    :param cart:
    :return:
    """
    if not request.method == 'POST':
        return HttpResponse(status=403)

    all_lines = cart.lines.all()
    line = [line for line in all_lines if line.data.get('is_eddm', False)
            and not line.data.get('eddm_resolved', True)]

    if len(line) > 0:
        # If exists a element not resolved, attach current routes info
        current_line = line[0]
        parsed = json.loads(request.body)
        data = {'routes': parsed.get('routes'), 'price': parsed.get('costs'),
                'count_total': parsed.get('total'), 'eddm_resolved': True}
        current_line.data = {**current_line.data, **data}
        current_line.save()
    else:
        # Attach this to session
        request.session['eddm_info'] = json.loads(request.body)

    return JsonResponse({'is_fallback': len(line) > 0}, status=201)
