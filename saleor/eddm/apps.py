from django.apps import AppConfig


class EddmConfig(AppConfig):
    name = 'eddm'
