from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('direction', views.direction, name='direction'),
    path('order', views.order, name='order'),
    path('reset', views.reset, name='reset')
]
