import json


def insert_routes_to_cartline(cart, eddm_info):
    all_lines = cart.lines.all()
    line = [line for line in all_lines if line.data.get('is_eddm', False)]

    if line:
        # If exists a element
        current_line = line[0]
        parsed = json.loads(eddm_info)
        data = {'routes': parsed.get('routes'), 'price': parsed.get('costs'),
                'count_total': parsed.get('total'), 'eddm_resolved': True}
        current_line.data = {**current_line.data, **data}
        current_line.save()
