import { Component } from 'react';
import { loadModules } from 'react-arcgis';
import PropTypes from 'prop-types';

class BaseGeoprocessor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            geoprocessor: null,
        };
        this.execute = this.execute.bind(this);
    }

    render() {
        return null;
    }

    componentWillMount() {
        loadModules(['esri/tasks/Geoprocessor']).then(([Geoprocessor]) => {
            let geoprocessor = new Geoprocessor({url: this.props.url});
            geoprocessor.outSpatialParameters = this.props.outSpatialParameters;
            this.setState({geoprocessor: geoprocessor});
        })
    }

    execute(params) {
        return this.state.geoprocessor.execute(params);
    }
}

BaseGeoprocessor.propTypes = {
    url: PropTypes.string.isRequired,
    outSpatialParameters: PropTypes.object
};

export default BaseGeoprocessor;