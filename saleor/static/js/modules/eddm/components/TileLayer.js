import { Component } from 'react';
import PropTypes from 'prop-types';
import { loadModules } from 'react-arcgis';

class TileLayer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            layer: null,
        }
    }

    render() {
        return null;
    }

    componentWillMount() {
        loadModules(["esri/layers/TileLayer"]).then(([TileLayer]) => {
            let layer = new TileLayer({url: this.props.url});
            this.props.map.add(layer);
            this.setState({layer: layer});
        });
    }

    componentWillUnmount() {
        this.props.map.remove(this.state.layer);
    }
}

TileLayer.propTypes = {
    url: PropTypes.string.isRequired
};

export default TileLayer;