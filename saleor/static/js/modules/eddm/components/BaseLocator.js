import { Component } from 'react';
import { loadModules } from 'react-arcgis';
import PropTypes from 'prop-types';

class BaseLocator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            locator: null,
        };
    }

    addressToLocations = () => {
        return this.state.locator.addressToLocations();
    };

    locationToAddress = () => {
        return this.state.locator.locationToAddress();
    };

    render() {
        return null;
    }

    componentWillMount() {
        loadModules(["esri/tasks/Locator"]).then(([Locator]) => {
            let locator = new Locator({url: this.props.url});
            this.setState({
                locator: locator
            });
        });
    }
}

BaseLocator.propTypes = {
    url: PropTypes.string.isRequired,
};

export default BaseLocator;