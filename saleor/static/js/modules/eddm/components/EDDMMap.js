import React, { Component } from 'react';
import TileLayer from './TileLayer';
import MapImageLayer from './MapImageLayer';
import GraphicsLayer from './GraphicsLayer';
import { SelectZIPRoute, SelectZIPBox, SelectNearRoute, SelectNearBox, SelectCityState } from './geoprocessors';
import { loadModules } from 'react-arcgis';
import PropTypes from 'prop-types';
import { Map } from 'react-arcgis';
class EDDMMap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedZipCode: "",
        };
        this.selectZIPR = React.createRef();
        this.selectZIPB = React.createRef();
        this.selectNearR = React.createRef();
        this.selectNearB = React.createRef();
        this.selectCity = React.createRef();
        this.cridLayer = React.createRef();
        this.defaultSymbol = null;
        this.selectedRoutes = {
            features: [],
        };
        this.initializeSymbols();
    }

    render() {
        return (
            <div>
                <div className="map-controls">
                    <input className="zip-code-field" type="text" value={this.state.selectedZipCode} onChange={this.handleChange}/>
                    <button className="search-button" onClick={this.searchZipcode}>Search</button>
                </div>
                <div className="map-container">
                    <Map loaderOptions={{
                        dojoConfig: {
                            has: {
                                'esri-promise-compatibility-deprecation-warnings': 0,
                                'esri-promise-compatibility': 0,
                            }
                        }
                    }}>
                        <TileLayer url="https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer"/>
                        <MapImageLayer url="https://gis.usps.com/arcgis/rest/services/EDDM/EDDM_ZIP5/MapServer"/>
                        <GraphicsLayer ref={this.cridLayer} id="cridLayer"/>
                        <SelectZIPRoute ref={this.selectZIPR} baseUrlService="https://gis.usps.com/arcgis/rest/services/"/>
                        <SelectZIPBox ref={this.selectZIPB} baseUrlService="https://gis.usps.com/arcgis/rest/services/"/>
                        <SelectNearRoute ref={this.selectNearR} baseUrlService="https://gis.usps.com/arcgis/rest/services/"/>
                        <SelectNearBox ref={this.selectNearB} baseUrlService="https://gis.usps.com/arcgis/rest/services/"/>
                        <SelectCityState ref={this.selectCity} baseUrlService="https://gis.usps.com/arcgis/rest/services/"/>
                    </Map>
                </div>
            </div>
        )
    }

    /**
     * Initialize ArcGIS symbols from server using loadModules (async).
     */
    initializeSymbols = () => {
        loadModules(["esri/symbols/SimpleLineSymbol", "esri/symbols/PictureMarkerSymbol"]).then(([SimpleLineSymbol, PictureMarkerSymbol]) => {
            this.routeSymbol = new SimpleLineSymbol({
                color: [88, 88, 88, 0.25],
                style: "solid",
                join: "round",
                width: 4
            });
            this.boxSymbol = new PictureMarkerSymbol({
                url: "https://gis.usps.com/eddm/assets/usps_eddm_po_orange_pin.png",
                contentType: "image/png",
                width: 25,
                height: 32,
                angle: 0,
                xoffset: 0,
                yoffset: 16
            });
        });
    };

    /**
     * Manage zipcode input changes.
     * @param event
     */
    handleChange = event => {
        this.setState({selectedZipCode: event.target.value});
    };

    /**
     * Search the location by ZipCode and show the routes.
     */
    searchZipcode = () => {
        this.executeProcessor(this.state.selectedZipCode);
    };

    /**
     * Execute route display processor
     * @param zipcode Select ZipCode
     */
    executeProcessor = zipcode => {
        let paramsRoute = {
            "ZIP": zipcode,
            "Rte_Box": "R",
            "UserName": "EDDM"
        };
        let paramsBox = {
            "ZIP": zipcode,
            "Rte_Box": "B",
            "UserName": "EDDM",
        };
        let pRoute = this.selectZIPR.current.execute(paramsRoute);
        let pBox = this.selectZIPB.current.execute(paramsBox);

        Promise.all([pRoute, pBox]).then(this.showRoutes);

        let event = {
            zipcode: zipcode,
        };

        this.props.onChangeZipCode(event);
    };

    /**
     * Promised function, this executes the promises for route info based in ZipCode
     * @param rRoute Promise of SelectZIPRoute
     * @param rBox Promise of SelectZIPBox
     */
    showRoutes = ([rRoute, rBox]) => {
        this.displayResults(rRoute.results, rRoute.messages);
        //this.displayBoxResults(rBox.results, rBox.messages);

        this.renderRoutes();
    };

    /**
     * Display results of SelectZIPRoute (polygons)
     * @param results
     * @param messages
     */
    displayResults = (results, messages) => {
        let thisRoute = [];
        let resultFeatures = results[0].value.features;

        for (let i = 0, il = resultFeatures.length; i < il; i++) {
            thisRoute = [];
            thisRoute.attributes = resultFeatures[i].attributes;
            thisRoute.attributes.hovered = false;
            thisRoute.attributes.graphic = resultFeatures[i];
            thisRoute.attributes.graphic.defaultSymbol = this.routeSymbol;

            this.selectedRoutes.features.push(thisRoute);
        }
    };

    /**
     *
     * @param results
     * @param messages
     */
    displayBoxResults = (results, messages) => {
        loadModules(["esri/geometry/SpatialReference", "esri/Graphic", "esri/geometry/Point"]).then(([SpatialReference, Graphic, Point]) => {
            //* Get featureset from ArcServer PO Box search results
            let thisRoute = [], i, il;
            let resultFeatures = results[0].value.features;

            for (i = 0, il = resultFeatures.length; i < il; i++) {
                let zipcrid = resultFeatures[i].attributes.ZIP_CODE + resultFeatures[i].attributes.CRID_ID;
                thisRoute = [];

                let point = new Point(resultFeatures[i].geometry.points[0], new SpatialReference({
                    wkid: 102100
                }));
                let graphic = new Graphic({geometry: point});
                thisRoute.attributes = [];
                thisRoute.attributes = resultFeatures[i].attributes;
                thisRoute.attributes.graphic = graphic;
                thisRoute.attributes.graphic.attributes = [];
                thisRoute.attributes.graphic.attributes = resultFeatures[i].attributes;
                thisRoute.attributes.graphic.attributes.hovered = [false];
                thisRoute.attributes.graphic.attributes.ZIP_CRID = [resultFeatures[i].attributes.ZIP_CRID];
                thisRoute.attributes.graphic.defaultSymbol = this.boxSymbol;
                this.selectedRoutes.features.push(thisRoute);

                // Call render
                this.renderRoutes();
            }
        });
    };

    renderRoutes = () => {
        this.selectedRoutes.features.forEach((feature) => {
            this.cridLayer.current.getLayer().add(feature.attributes.graphic);
        });
        this.cridLayer.current.getLayer().graphics.forEach((graphic) => {
            graphic.symbol = graphic.defaultSymbol;
        })
    }
}

EDDMMap.propTypes = {
    onChangeZipCode: PropTypes.func.isRequired,
};

export default EDDMMap;
