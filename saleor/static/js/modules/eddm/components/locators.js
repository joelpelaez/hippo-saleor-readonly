import React, { Component } from 'react';
import BaseLocator from './BaseLocator';
import PropTypes from 'prop-types';

export class Geolocator extends Component {
    constructor(props) {
        super(props);
        this.locator = React.createRef();
    }

    render() {
        return (
            <BaseLocator url={this.props.baseUrlService + "/locators/US_Street/GeocodeServer"}/>
        )
    }
}

Geolocator.propTypes = {
    baseUrlService: PropTypes.string.isRequired,
};