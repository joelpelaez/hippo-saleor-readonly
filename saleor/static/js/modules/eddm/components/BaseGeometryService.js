import React, { Component } from 'react';
import { loadModules } from 'react-arcgis';
import PropTypes from 'prop-types';

class BaseGeometryService extends Component {
    constructor(props) {
        super(props);
        this.state = {
            geometryService: null,
        };
    }

    addressToLocations = () => {
        return this.state.locator.addressToLocations();
    };

    locationToAddress = () => {
        return this.state.locator.locationToAddress();
    };

    render() {
        return null;
    }

    componentWillMount() {
        loadModules(["esri/tasks/GeometryService"]).then(([GeometryService]) => {
            let geometryService = new GeometryService({url: this.props.url});
            this.setState({
                geometryService: geometryService
            });
        });
    }
}

BaseGeometryService.propTypes = {
    url: PropTypes.string.isRequired,
};

export default BaseGeometryService;