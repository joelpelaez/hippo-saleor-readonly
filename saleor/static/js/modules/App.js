import React, { Component } from 'react';
import EDDMMap from './eddm/components/EDDMMap';

class App extends Component {
    showZipCode(event) {
        console.log(event.zipcode);
    }
    render() {
        return (
            <div>
                <div  style={{
  height: "400px",
  width: "600px"
}}>
                    <EDDMMap onChangeZipCode={this.showZipCode}/>
                </div>
            </div>
        );
    }
}

export default App;
