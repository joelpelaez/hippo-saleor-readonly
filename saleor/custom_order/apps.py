from django.apps import AppConfig


class CustomOrderConfig(AppConfig):
    name = 'custom_order'
