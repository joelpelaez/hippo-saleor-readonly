import React, { Component } from 'react';
import EDDMMap from './components/EDDMMap';
import './App.css';

class App extends Component {
  static showZipCode(event) {
    console.log(event.zipcode);
  }
  render() {
    return (
      <div className="map-div">
        <EDDMMap onChangeZipCode={this.showZipCode} zoom={10}/>
      </div>
    );
  }
}

export default App;
