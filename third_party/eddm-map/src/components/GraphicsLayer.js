import { Component } from 'react';
import PropTypes from 'prop-types';
import { loadModules } from 'react-arcgis';

class GraphicsLayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      layer: null
    };
  }

    getLayer = () => {
      return this.state.layer;
    };

    render() {
      return null;
    }

    componentWillMount() {
      loadModules(['esri/layers/GraphicsLayer']).then(([GraphicsLayer]) => {
        let layer = new GraphicsLayer({id: this.props.id});
        this.props.map.add(layer);
        this.setState({layer: layer});
      });
    }

    componentWillUnmount() {
      this.props.map.remove(this.state.layer);
    }
}

GraphicsLayer.propTypes = {
  id: PropTypes.string.isRequired
};

export default GraphicsLayer;
