import { Component } from 'react';
import PropTypes from 'prop-types';
import { loadModules } from 'react-arcgis';

class MapImageLayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      layer: null
    };
  }

  render() {
    return null;
  }

    getLayer = () => {
      return this.state.layer;
    };

    componentWillMount() {
      loadModules(['esri/layers/MapImageLayer']).then(([MapImageLayer]) => {
        let layer = new MapImageLayer({url: this.props.url});
        this.props.map.add(layer);
        this.setState({layer: layer});
      });
    }

    componentWillUnmount() {
      this.props.map.remove(this.state.layer);
    }
}

MapImageLayer.propTypes = {
  url: PropTypes.string.isRequired
};

export default MapImageLayer;
