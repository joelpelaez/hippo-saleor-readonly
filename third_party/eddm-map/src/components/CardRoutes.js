import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

axios.defaults.xsrfHeaderName = 'X-CSRFToken';
axios.defaults.xsrfCookieName = 'csrftoken';

class CardRoutes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: '',
      eddmUrl: ''
    };
    this.sendLocations = this.sendLocations.bind(this);
  }

  sendLocations() {
    let routes = {
      routes: this.props.routes.filter(route => route.active).map(route => {
        return {
          id: route.attributes.ZIP_CRID,
          zip: route.attributes.ZIP_CODE,
          crid: route.attributes.CRID_ID,
          count: route.attributes.TOT_CNT,
          price: (route.attributes.TOT_CNT * 0.21).toFixed(2)
        };
      }),
      total: this.props.homes,
      costs: (this.props.homes * 0.21).toFixed(2)
    };
    axios.post(this.state.url, routes).then(value => {
      if (value.data.is_fallback === true) {
        window.location.href = '/checkout';
      } else {
        window.location.href = this.state.eddmUrl;
      }
    }).catch(reason => {
      console.log(reason);
    });
  }

  render() {
    const { routes, homes, costs } = this.props;
    const selectedRoutes = routes.filter(route => route.active);

    return (
      <div className="col-sm-12 col-md-12 col-lg-4 col-xl-4 mb-4">
        <div className="card bg-light">
          <div className="card-body">
            <h5 className="card-title">Order Details</h5>
            <p className="card-text">Selected Routes: </p>
            <ul>
              {selectedRoutes.map((route, index) => (
                <li key={index}>
                  {route.attributes.ZIP_CODE}-{route.attributes.CRID_ID}
                </li>
              ))}
            </ul>
            <p className="card-text">Total Homes: {homes} </p>
            <p className="card-text">Exclude Business:</p>
            <p className="card-text">Total Cost: {(costs * 0.21).toFixed(2)}</p>
            <button className="btn primary" onClick={this.sendLocations}>
              Next
            </button>
          </div>
        </div>
      </div>
    );
  }

  componentDidMount() {
    this.setState({
      url: document.getElementById('root').dataset.url,
      eddmUrl: document.getElementById('root').dataset.eddmUrl
    });
  }
}

CardRoutes.propTypes = {
  routes: PropTypes.array.isRequired,
  homes: PropTypes.number.isRequired,
  costs: PropTypes.number.isRequired
};

export default CardRoutes;
