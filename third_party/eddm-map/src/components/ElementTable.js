import React from 'react';

class ElementTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false
    };
  }

  handleTableClick = (event) => {
    event.preventDefault();
    this.setState((prevState, props) => ({
      active: !prevState.active
    }));
  };

  render() {
    const {route, actived} = this.props;
    return (
      <tr className={actived ? 'table-primary' : ''} >
        <th scope="row">{route.attributes.ZIP_CODE}-{route.attributes.CRID_ID}</th>
        <td>{route.attributes.RES_CNT}</td>
        <td>{route.attributes.BUS_CNT}</td>
        <td>{route.attributes.TOT_CNT}</td>
        <td>{(route.attributes.AVG_HH_SIZ).toFixed(2)}</td>
        <td>{route.attributes.MED_INCOME}</td>
        <td>${(route.attributes.TOT_CNT * 0.21).toFixed(2)}</td>
      </tr>
    );
  }
}

export default ElementTable;
