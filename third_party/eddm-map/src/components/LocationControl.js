import React from 'react';

class LocationControl extends React.Component {
  render() {
    return null;
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    if (this.props.center[0] !== nextProps.center[0] ||
            this.props.center[1] !== nextProps.center[1]) {
      this.props.view.goTo(nextProps.center);
      return true;
    }

    return false;
  }
}

export default LocationControl;
