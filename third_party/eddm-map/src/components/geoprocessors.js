import React, { Component } from 'react';
import BaseGeoprocessor from './BaseGeoprocessor';
import PropTypes from 'prop-types';

export class SelectZIPRoute extends Component {
  constructor(props) {
    super(props);
    this.geoprocessor = React.createRef();
  }

    execute = params => this.geoprocessor.current.execute(params);

    render() {
      return (
        <BaseGeoprocessor ref={this.geoprocessor} url={this.props.baseUrlService + '/EDDM/selectZIP/GPServer/routes'}
          outSpatialParameters={{'wkid': 102100}}/>
      );
    }
}

SelectZIPRoute.propTypes = {
  baseUrlService: PropTypes.string.isRequired
};

export class SelectZIPBox extends Component {
  constructor(props) {
    super(props);
    this.geoprocessor = React.createRef();
  }

    execute = params => this.geoprocessor.current.execute(params);

    render() {
      return (
        <BaseGeoprocessor ref={this.geoprocessor} url={this.props.baseUrlService + '/EDDM/selectZIP/GPServer/boxes'}
          outSpatialParameters={{'wkid': 102100}}/>
      );
    }
}

SelectZIPBox.propTypes = {
  baseUrlService: PropTypes.string.isRequired
};

export class SelectNearRoute extends Component {
  constructor(props) {
    super(props);
    this.geoprocessor = React.createRef();
  }

    execute = params => this.geoprocessor.current.execute(params);

    render() {
      return (
        <BaseGeoprocessor ref={this.geoprocessor} url={this.props.baseUrlService + '/EDDM/selectNear/GPServer/routes'}
          outSpatialParameters={{'wkid': 102100}}/>
      );
    }
}

SelectNearRoute.propTypes = {
  baseUrlService: PropTypes.string.isRequired
};

export class SelectNearBox extends Component {
  constructor(props) {
    super(props);
    this.geoprocessor = React.createRef();
  }

    execute = params => this.geoprocessor.current.execute(params);

    render() {
      return (
        <BaseGeoprocessor ref={this.geoprocessor} url={this.props.baseUrlService + '/EDDM/selectNear/GPServer/boxes'}
          outSpatialParameters={{'wkid': 102100}}/>
      );
    }
}

SelectNearBox.propTypes = {
  baseUrlService: PropTypes.string.isRequired
};

export class SelectCityState extends Component {
  constructor(props) {
    super(props);
    this.geoprocessor = React.createRef();
  }

    execute = params => this.geoprocessor.current.execute(params);

    render() {
      return (
        <BaseGeoprocessor ref={this.geoprocessor} url={this.props.baseUrlService + '/EDDM/selectNear/GPServer/boxes'}
          outSpatialParameters={{'wkid': 102100}}/>
      );
    }
}

SelectCityState.propTypes = {
  baseUrlService: PropTypes.string.isRequired
};
