import React, { Component } from 'react';
import TileLayer from './TileLayer';
import MapImageLayer from './MapImageLayer';
import GraphicsLayer from './GraphicsLayer';
import { SelectZIPRoute, SelectZIPBox, SelectNearRoute, SelectNearBox, SelectCityState } from './geoprocessors';
import { Geolocator } from './locators';
import EventHandler from './EventHandler';
import { loadModules, Map } from 'react-arcgis';
import PropTypes from 'prop-types';
import LocationControl from './LocationControl';
import './EDDMMap.css';
import MapAccesor from './MapAccesor';
import ViewAccesor from './ViewAccesor';
import { Row, Col, Table, Button, Input, FormGroup, Form } from 'reactstrap';
import ElementTable from './ElementTable';
import CardRoutes from './CardRoutes';
import { addRoute, addCost, addHomes, deleteRoute, subtractCost, loadRoutesList, subtractHome, activeRoute, deactiveRoute } from '../actions';
import { connect } from 'react-redux';

class EDDMMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedZipCode: '',
      center: [-122.4443, 47.2529],
      routes: [],
      selectedRoutes: [],
      homes: 0,
      costs: 0
    };
    this.selectZIPR = React.createRef();
    this.selectZIPB = React.createRef();
    this.selectNearR = React.createRef();
    this.selectNearB = React.createRef();
    this.selectCity = React.createRef();
    this.cridLayer = React.createRef();
    this.geolocator = React.createRef();
    this.mapAccesor = React.createRef();
    this.viewAccesor = React.createRef();
    this.selectedRoutes = {
      features: []
    };
    this.initializeSymbols();
  }

  render() {
    return (
      <React.Fragment>
        <Form>
          <FormGroup className="map-controls row">
            <Col sm={12} md={6} lg={6}>
              <Input className="zip-code-field" placeholder="Enter Zip Code" type="text" value={this.state.selectedZipCode} onChange={this.handleChange}/>
            </Col>
            <Col sm={12} md={3} lg={3}>
              <Button className="search-button primary" onClick={this.searchZipcode}>Search</Button>
            </Col>
          </FormGroup>
        </Form>
        <Row>
          <Col sm={12} md={12} lg={8} xl={8} >
            <Table responsive hover >
              <thead className="thead-dark">
                <tr>
                  <th scope="col">Route</th>
                  <th scope="col">Residential</th>
                  <th scope="col">Business</th>
                  <th scope="col">Total</th>
                  <th scope="col">Size</th>
                  <th scope="col">Income</th>
                  <th scope="col">Cost</th>
                </tr>
              </thead>
              <tbody>
                {
                  this.props.routes.map(
                    (route, index) => {
                      return (
                        <ElementTable route={route} key={index} actived={!!route.active}/>
                      );
                    }
                  )
                }
              </tbody>
            </Table>
          </Col>
          <CardRoutes routes={this.props.routes} costs={this.props.costs} homes={this.props.homes} />
        </Row>
        <div className="map-container">
          <Map loaderOptions={{
            dojoConfig: {
              has: {
                'esri-promise-compatibility-deprecation-warnings': 0,
                'esri-promise-compatibility': 0
              }
            }
          }} viewProperties={{
            zoom: this.props.zoom || 10
          }}
          >
            <MapAccesor ref={this.mapAccesor}/>
            <ViewAccesor ref={this.viewAccesor}/>
            <LocationControl center={this.state.center}/>
            <EventHandler event="click" callback={this.onClick}/>
            <EventHandler event="pointer-move" callback={this.onPointerMove}/>
            <EventHandler event="pointer-down" callback={this.onPointerMove}/>
            <TileLayer url="https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer"/>
            <MapImageLayer url="https://gis.usps.com/arcgis/rest/services/EDDM/EDDM_ZIP5/MapServer"/>
            <Geolocator ref={this.geolocator} baseUrlService="https://gis.usps.com/arcgis/rest/services/"/>
            <GraphicsLayer ref={this.cridLayer} id="cridLayer"/>
            <SelectZIPRoute ref={this.selectZIPR} baseUrlService="https://gis.usps.com/arcgis/rest/services/"/>
            <SelectZIPBox ref={this.selectZIPB} baseUrlService="https://gis.usps.com/arcgis/rest/services/"/>
            <SelectNearRoute ref={this.selectNearR} baseUrlService="https://gis.usps.com/arcgis/rest/services/"/>
            <SelectNearBox ref={this.selectNearB} baseUrlService="https://gis.usps.com/arcgis/rest/services/"/>
            <SelectCityState ref={this.selectCity} baseUrlService="https://gis.usps.com/arcgis/rest/services/"/>
          </Map>
        </div>
      </React.Fragment>
    );
  }

    onClick = (event, view) => {
      let promise = view.hitTest(event);
      promise = promise.then(response => {
        let results = response.results.filter(result => {
          return result.graphic.layer === this.cridLayer.current.getLayer();
        });

        if (results.length === 0) { return; }

        let graphic = results[0].graphic;

        if (graphic !== undefined) {
          if (graphic.selected) {
            graphic.symbol = graphic.offSymbol;
            graphic.selected = false;

            this.props.deactiveRoute(graphic.attributes.ZIP_CRID);
            this.props.subtractCost(graphic.attributes.TOT_CNT);
            this.props.subtractHome(graphic.attributes.RES_CNT);
          } else {
            graphic.symbol = graphic.selectedSymbol;
            graphic.selected = true;
            graphic.hovered = false;

            this.props.activeRoute(graphic.attributes.ZIP_CRID);
            this.props.addHome(graphic.attributes.RES_CNT);
            this.props.addCost(graphic.attributes.TOT_CNT);
          }
        }
      }, error => {
         console.log(error);
       });
    };

    onPointerMove = (event, view) => {
      let promise = view.hitTest(event);
      promise.then(response => {
        let results = response.results.filter(result => {
          return result.graphic.layer === this.cridLayer.current.getLayer();
        });

        if (results.length === 0) {
          this.cridLayer.current.getLayer().graphics.forEach(grap => {
            if (grap.hovered) {
              grap.symbol = grap.prevSymbol;
              grap.hovered = false;
            }
          });

          return;
        }

        let graphic = results[0].graphic;

        if (!graphic.hovered && !graphic.selected) {
          this.cridLayer.current.getLayer().graphics.forEach(grap => {
            if (grap.hovered) {
              grap.symbol = grap.prevSymbol;
              grap.hovered = false;
            }
          });

          graphic.prevSymbol = graphic.symbol;
          graphic.symbol = graphic.hoveredSymbol;
          graphic.hovered = true;
        }
      }, reason => {
        console.error(reason);
      });
    };

    /**
     * Initialize ArcGIS symbols from server using loadModules (async).
     */
    initializeSymbols = () => {
      loadModules(['esri/symbols/SimpleLineSymbol', 'esri/symbols/PictureMarkerSymbol']).then(([SimpleLineSymbol, PictureMarkerSymbol]) => {
        this.routeSymbol = new SimpleLineSymbol({
          color: [88, 88, 88, 0.4],
          style: 'solid',
          join: 'round',
          width: 4
        });
        this.routeOff = new SimpleLineSymbol({
          color: [100, 100, 100, 0.4],
          style: 'solid',
          join: 'round',
          width: 4
        });
        this.routeHovered = new SimpleLineSymbol({
          color: [163, 91, 223, 0.4],
          style: 'solid',
          join: 'round',
          width: 4
        });
        this.routeSelected = new SimpleLineSymbol({
          color: [255, 107, 29, 0.4],
          style: 'solid',
          join: 'round',
          width: 4
        });
        this.boxSymbol = new PictureMarkerSymbol({
          url: 'https://gis.usps.com/eddm/assets/usps_eddm_po_orange_pin.png',
          contentType: 'image/png',
          width: 25,
          height: 32,
          angle: 0,
          xoffset: 0,
          yoffset: 16
        });
      });
    };

    getLocation = () => {
      if ('geolocation' in navigator) {
        navigator.geolocation.getCurrentPosition(position => {
          let latitude = position.coords.latitude;
          let longitude = position.coords.longitude;

          this.setState({center: [longitude, latitude]});
        }, error => {
          switch (error.code) {
            case error.PERMISSION_DENIED:
              console.log('Permission denied: ' + error.message);
              break;
            case error.POSITION_UNAVAILABLE:
              console.log('Cannot get position: ' + error.message);
              break;
            case error.TIMEOUT:
              console.log('Try again');
              break;
            default:
              break;
          }
        });
      }
    };

    /**
     * Manage zipcode input changes.
     * @param event
     */
    handleChange = event => {
      event.preventDefault();
      this.setState({selectedZipCode: event.target.value});
    };

    /**
     * Search the location by ZipCode and show the routes.
     */
    searchZipcode = () => {
      this.executeProcessor(this.state.selectedZipCode);
    };

    /**
     * Execute route display processor
     * @param zipcode Select ZipCode
     */
    executeProcessor = zipcode => {
      let paramsRoute = {
        'ZIP': zipcode,
        'Rte_Box': 'R',
        'UserName': 'EDDM'
      };
      let paramsBox = {
        'ZIP': zipcode,
        'Rte_Box': 'B',
        'UserName': 'EDDM'
      };
      let pRoute = this.selectZIPR.current.execute(paramsRoute);
      let pBox = this.selectZIPB.current.execute(paramsBox);

      Promise.all([pRoute, pBox]).then(this.showRoutes);
    };

    /**
     * Promised function, this executes the promises for route info based in ZipCode
     * @param rRoute Promise of SelectZIPRoute
     * @param rBox Promise of SelectZIPBox
     */
    showRoutes = ([rRoute, rBox]) => {
      this.displayResults(rRoute.results, rRoute.messages);
      this.displayBoxResults(rBox.results, rBox.messages);

      this.renderRoutes();
    };

    /**
     * Display results of SelectZIPRoute (polygons)
     * @param results
     * @param messages
     */
    displayResults = (results, messages) => {
      let thisRoute = [];
      let resultFeatures = results[0].value.features;

      for (let i = 0, il = resultFeatures.length; i < il; i++) {
        thisRoute = [];
        thisRoute.attributes = resultFeatures[i].attributes;
        thisRoute.attributes.graphic = resultFeatures[i];
        thisRoute.attributes.graphic.hovered = false;
        thisRoute.attributes.graphic.defaultSymbol = this.routeOff;
        thisRoute.attributes.graphic.offSymbol = this.routeOff;
        thisRoute.attributes.graphic.hoveredSymbol = this.routeHovered;
        thisRoute.attributes.graphic.selectedSymbol = this.routeSelected;

        this.selectedRoutes.features.push(thisRoute);
      }
    };

    /**
     *
     * @param results
     * @param messages
     */
    displayBoxResults = (results, messages) => {
      loadModules(['esri/geometry/SpatialReference', 'esri/Graphic', 'esri/geometry/Point']).then(([SpatialReference, Graphic, Point]) => {
        //* Get featureset from ArcServer PO Box search results
        let thisRoute = [], i, il;
        let resultFeatures = results[0].value.features;

        for (i = 0, il = resultFeatures.length; i < il; i++) {
          thisRoute = [];

          let point = new Point(resultFeatures[i].geometry.points[0], new SpatialReference({
            wkid: 102100
          }));
          let graphic = new Graphic({geometry: point});
          thisRoute.attributes = [];
          thisRoute.attributes = resultFeatures[i].attributes;
          thisRoute.attributes.graphic = graphic;
          thisRoute.attributes.graphic.attributes = [];
          thisRoute.attributes.graphic.attributes = resultFeatures[i].attributes;
          thisRoute.attributes.graphic.defaultSymbol = this.boxSymbol;

          this.selectedRoutes.features.push(thisRoute);
        }
      });
    };

    renderRoutes = () => {
      this.selectedRoutes.features.forEach((feature) => {
        this.cridLayer.current.getLayer().add(feature.attributes.graphic);
      });
      this.cridLayer.current.getLayer().graphics.forEach((graphic) => {
        graphic.symbol = graphic.defaultSymbol;
      });
      console.log(this.selectedRoutes.features);
      const arr = this.selectedRoutes.features.map((route) => {
        return {
          ...route,
          active: false
        };
      });
      this.props.loadRoutesList(arr);
      /* this.setState((prevState, props) => ({
            ...prevState,
            routes: this.selectedRoutes.features
        })); */

      loadModules(['esri/geometry/Extent']).then(([Extent]) => {
        let newExtent, geometry, curExtent;
        this.cridLayer.current.getLayer().graphics.forEach(graphic => {
          geometry = graphic.geometry;
          if (geometry.declaredClass === 'esri.geometry.Polyline') {
            if (newExtent) {
              newExtent = newExtent.union(geometry.extent);
            } else {
              newExtent = geometry.extent;
            }
          } else {
            if (newExtent) {
              newExtent.union(new Extent(geometry.x - 1, geometry.y - 1, geometry.x + 1, geometry.y + 1, geometry.spatialReference));
            } else {
              newExtent = new Extent(geometry.x - 1, geometry.y - 1, geometry.x + 1, geometry.y + 1, geometry.spatialReference);
            }
          }
        });

        if (newExtent) {
          this.viewAccesor.current.getView().goTo(newExtent);
        }
      });
    }
}

EDDMMap.propTypes = {
  zoom: PropTypes.number.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    routes: state.routes,
    selectedRoutes: state.selectedRoutes,
    homes: state.homes,
    costs: state.costs
  };
}
function mapDispatchToProps(dispatch) {
  return {
    addRoute: (route) => dispatch(addRoute(route)),
    deleteRoute: (index) => dispatch(deleteRoute(index)),
    addHome: (home) => dispatch(addHomes(home)),
    subtractHome: (home) => dispatch(subtractHome(home)),
    addCost: (cost) => dispatch(addCost(cost)),
    subtractCost: (cost) => dispatch(subtractCost(cost)),
    loadRoutesList: (routes) => dispatch(loadRoutesList(routes)),
    activeRoute: (route) => dispatch(activeRoute(route)),
    deactiveRoute: (route) => dispatch(deactiveRoute(route))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EDDMMap);
