import React, { Component } from 'react';
import BaseLocator from './BaseLocator';
import PropTypes from 'prop-types';

export class Geolocator extends Component {
  constructor(props) {
    super(props);
    this.locator = React.createRef();
  }

    addressToLocations = params => {
      return this.locator.current.addressToLocations(params);
    };

    locationToAddress = params => {
      return this.locator.current.locationToAddress(params);
    };

    render() {
      return (
        <BaseLocator ref={this.locator} url={this.props.baseUrlService + '/locators/US_Street/GeocodeServer'}/>
      );
    }
}

Geolocator.propTypes = {
  baseUrlService: PropTypes.string.isRequired
};
