import React from 'react';
import PropTypes from 'prop-types';

class EventHandler extends React.Component {
  render() {
    return null;
  }

  componentDidMount() {
    this.props.view.on(this.props.event, this.baseCallback);
  }

    baseCallback = event => {
      return this.props.callback(event, this.props.view);
    };
}

EventHandler.propTypes = {
  event: PropTypes.string.isRequired,
  callback: PropTypes.func.isRequired
};

export default EventHandler;
