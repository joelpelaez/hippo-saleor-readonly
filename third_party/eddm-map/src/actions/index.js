import {ADD_ROUTE, ADD_COST, ADD_HOME, DELETE_ROUTE, LOAD_ROUTES, SUBTRACT_COST, SUBTRACT_HOME, ACTIVE_ROUTE, DEACTIVE_ROUTE} from '../actions-types';
export const addRoute = (route) => {
  return {
    type: ADD_ROUTE,
    payload: route
  };
};
export const deleteRoute = (index) => {
  return {
    type: DELETE_ROUTE,
    payload: index
  };
};
export const loadRoutesList = (routes) => {
  return {
    type: LOAD_ROUTES,
    payload: routes
  };
};

export const addCost = (cost) => {
  return {
    type: ADD_COST,
    payload: cost
  };
};

export const subtractCost = (cost) => {
  return {
    type: SUBTRACT_COST,
    payload: cost
  };
};

export const addHomes = (home) => {
  return {
    type: ADD_HOME,
    payload: home
  };
};
export const subtractHome = (home) => {
  return {
    type: SUBTRACT_HOME,
    payload: home
  };
};

export const activeRoute = (index) => {
  return {
    type: ACTIVE_ROUTE,
    payload: index
  };
};
export const deactiveRoute = (index) => {
  return {
    type: DEACTIVE_ROUTE,
    payload: index
  };
};
