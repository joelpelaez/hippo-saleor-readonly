import {
  ADD_ROUTE,
  ADD_COST,
  ADD_HOME,
  DELETE_ROUTE,
  LOAD_ROUTES,
  SUBTRACT_COST,
  SUBTRACT_HOME,
  ACTIVE_ROUTE, DEACTIVE_ROUTE
} from '../actions-types';

const initialState = {
  isLoading: true,
  routes: [],
  homes: 0,
  costs: 0
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_ROUTES:
      return {
        ...state,
        routes: [ ...state.routes, ...action.payload ]
      };
    case ACTIVE_ROUTE:
      return {
        ...state,
        routes: state.routes.map((route) => {
          if (route.attributes.ZIP_CRID === action.payload) {
            return {
              ...route,
              active: true
            };
          } else {
            return {
              ...route
            };
          }
        })
      };
    case DEACTIVE_ROUTE:
      return {
        ...state,
        routes: state.routes.map((route) => {
          if (route.attributes.ZIP_CRID === action.payload) {
            return {
              ...route,
              active: false
            };
          } else {
            return {
              ...route
            };
          }
        })
      };
    case ADD_ROUTE:
      return {
        ...state,
        selectedRoutes: [...state.selectedRoutes, { ...action.payload } ]
      };
    case DELETE_ROUTE:
      return {
        ...state,
        selectedRoutes: state.selectedRoutes.filter((route) => route.ZIP_CRID !== action.payload)
      };
    case ADD_COST:
      return {
        ...state,
        costs: state.costs + action.payload
      };
    case SUBTRACT_COST:
      return {
        ...state,
        costs: state.costs - action.payload
      };
    case ADD_HOME:
      return {
        ...state,
        homes: state.homes + action.payload
      };
    case SUBTRACT_HOME:
      return {
        ...state,
        homes: state.homes - action.payload
      };

    default:
      return state;
  }
};
